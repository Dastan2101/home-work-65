import React, {Component} from 'react';
import axios from '../../axios-pages';
import './MainPage.css';
import Spinner from "../../components/UI/Spinner/Spinner";


class MainPage extends Component {

    state = {
        page: null
    };


    componentDidMount() {
        axios.get(`pages/${this.props.match.params.name}.json`).then(response => {
            this.setState({page: response.data})
        });
    }

    componentDidUpdate(prevProps) {

        if (this.props.match.params.name !== prevProps.match.params.name) {
            axios.get('pages/' + this.props.match.params.name + '.json').then(response => {
                this.setState({page: response.data});
            });
        }

    }

    render() {
        if (!this.state.page) {
            return <Spinner/>

        }

        return (
            <div className="Main-Page">
                <h1>{this.state.page.title}</h1>
                <h3>{this.state.page.content}</h3>
            </div>
        );
    }
}

export default MainPage;