import React, {Component} from 'react';
import Layout from "../components/Layout/Layout";
import MainPage from "./MainPage/MainPage";
import {Switch, Route} from "react-router-dom";
import ChangePage from "../components/ChangePage/ChangePage";


class App extends Component {

    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/pages/admin" component={ChangePage}/>
                    <Route path="/pages/:name" component={MainPage}/>
                    <Route render={() => <p>Start content</p>}/>
                </Switch>

            </Layout>
        );
    }
}

export default App;
