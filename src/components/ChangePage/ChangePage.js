import React, {Component} from 'react';
import axios from '../../axios-pages';
import {Button, FormGroup, Input, Label} from "reactstrap";
import Spinner from "../UI/Spinner/Spinner";
import './ChangePage.css';

class ChangePage extends Component {

    state = {
        pages: null,
        page: '',
        title: '',
        content: ''

    };

    componentDidMount() {
        axios.get('pages.json').then(response => {
            let pages = Object.keys(response.data);
            this.setState({pages: pages, page: pages[0]})
        })

    }

    changeHandler = event => {
        const page = event.target.name;
        this.setState({[page]: event.target.value})
    };

    componentDidUpdate(prevProps, prevState) {
        if (this.state.page !== prevState.page) {
            axios.get('pages/' + this.state.page + '.json').then(response => {
                this.setState({title: response.data.title, content: response.data.content})
            })
        }
    };

    saveChanged = (event) => {
        event.preventDefault();

        let change = {
            title: this.state.title,
            content: this.state.content,
        };

        axios.put('pages/' + this.state.page + '.json', change).then(() => {
            this.props.history.push('/pages/' + this.state.page)

        })
    };


    render() {
        if (!this.state.pages) {
            return <Spinner/>

        }

        return (
            <div className="form-page">
                <form onSubmit={this.saveChanged}>
                    <FormGroup>
                        <Label>Title</Label>
                        <Input value={this.state.title} onChange={this.changeHandler} name="title"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Content</Label>
                        <Input value={this.state.content} onChange={this.changeHandler} name="content"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Select</Label>
                        <Input type="select" name="page" id="page"
                               onChange={this.changeHandler}
                               value={this.state.page}
                        >
                            {this.state.pages.map((page, id) => (
                                <option key={id} value={page}>{page}</option>
                            ))}
                        </Input>
                    </FormGroup>
                    <Button color="info">Save</Button>
                </form>
            </div>
        );
    }
}

export default ChangePage;