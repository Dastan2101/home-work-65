import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";
import './NavigationItems.css';

const NavigationItems = () => (
    <ul className="NavigationItems">
        <NavigationItem to="/pages/home" exact>Home</NavigationItem>
        <NavigationItem to="/pages/about">About</NavigationItem>
        <NavigationItem to="/pages/contacts">Contacts</NavigationItem>
        <NavigationItem to="/pages/team">Team</NavigationItem>
        <NavigationItem to="/pages/works">Works</NavigationItem>
        <NavigationItem to="/pages/projects">Projects</NavigationItem>
        <NavigationItem to="/pages/admin">Admin</NavigationItem>
    </ul>
);

export default NavigationItems;